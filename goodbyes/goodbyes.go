package goodbyes

import "fmt"

func Goodbye(name string) string {
	message := fmt.Sprintf("Goodbye, %s", name)
	return message
}